CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Maintainers

INTRODUCTION
------------

The WebAuthn Authenticator module provides a login function using WebAuthn.

 - For a full description of the module, visit the project page:
   https://www.drupal.org/project/webauthn_authenticator

 - To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/webauthn_authenticator

REQUIREMENTS
------------

This module requires the following libraries:

 - web-auth/webauth-lib (https://packagist.org/packages/web-auth/webauthn-lib)
 - PHP ext-gmp (https://www.php.net/manual/book.gmp.php)

INSTALLATION
------------

 - Install as you would normally install a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Configure the permissions in
   Administration » People » Permissions (`/admin/people/permissions`):

   - Administer users (User module)

     Allows users to confirm and register anyone's WebAuthn credentials

   - Use webauthn authenticator

     Allows users to register WebAuthn credentials and login using WebAuthn.

USAGE
-----
* Register a WebAuthn credential
    * Go to `/user/{uid}/` and click on the `Webauthn Credentials`,
      or directly go to `/user/{uid}/webauthn-authenticator`.
      (If the page doesn't appear, make sure you have a right permission.)
    * Click on `Registration WebAuthn credential` button.
    * Register the label and security key.
    * If the credential was successfully made,
      the credential will be appear on the list.

* Login with WebAuthn

   * To use WebAuthn credentials,
     enter only your user name and leave the password empty.
     (After clicking on login,
      a dialog will appear asking you to read your security key.)
   * You can also log in with a username and password combination as usual.

Maintainers
-----------

Current maintainers:

 - Motoki Kobayashi (otofu) - https://www.drupal.org/u/otofu
