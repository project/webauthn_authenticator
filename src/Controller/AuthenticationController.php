<?php

namespace Drupal\webauthn_authenticator\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\webauthn_authenticator\Repository\PublicKeyCredentialSourceRepository;
use Drupal\webauthn_authenticator\Services\RpServer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Webauthn\PublicKeyCredentialRequestOptions;
use Webauthn\PublicKeyCredentialUserEntity;

/**
 * WebAuthn AuthenticationController.
 */
class AuthenticationController extends ControllerBase {

  /**
   * WebAuthn RP Server.
   *
   * @var \Drupal\webauthn_authenticator\Services\RpServer
   */
  private $rpServer;

  /**
   * The Private Temp Store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private $tempstore;

  /**
   * Constructor.
   *
   * @param \Drupal\webauthn_authenticator\Services\RpServer $rp_server
   *   WebAuthn RP Server.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore
   *   The private tempstore.
   */
  public function __construct(RpServer $rp_server, PrivateTempStoreFactory $tempstore) {
    $this->rpServer = $rp_server;
    $this->tempstore = $tempstore->get('webauthn_authenticator');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webauthn_authenticator.rp_server'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Get Server side Authentication options.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Server side Authentication options.
   */
  public function getAuthenticationOptions(Request $request) {
    $status_code = 200;

    try {
      $username = $request->query->get('username');
      $user = $this->userLoadByName($username);
      if (!$user) {
        throw new \Exception('You are not a registered user.', 403);
      }
      $user_entity = new PublicKeyCredentialUserEntity($user->getAccountName(), $user->id(), $user->getDisplayName());

      $credential_source_repository = new PublicKeyCredentialSourceRepository();
      $credential_sources = $credential_source_repository->findAllForUserEntity($user_entity);
      $allowed_credentials = array_map(function ($credential) {
        /** @var \Webauthn\PublicKeyCredentialSource $credential */
        return $credential->getPublicKeyCredentialDescriptor();
      }, $credential_sources);

      $credential_options = $this->rpServer->generatePublicKeyCredentialRequestOptions(PublicKeyCredentialRequestOptions::USER_VERIFICATION_REQUIREMENT_PREFERRED, $allowed_credentials);
      $response = $credential_options;

      $this->tempstore->set('credential_request_user', $user_entity);
      $this->tempstore->set('credential_request_options', $credential_options);
    }
    catch (\Exception $e) {
      $status_code = $e->getCode() > 500 ? 500 : $e->getCode();
      $response = [
        'status' => 'NG',
        'message' => $e->getMessage(),
      ];
    }

    return new JsonResponse($response, $status_code);
  }

  /**
   * Get active user by username.
   *
   * @param string $username
   *   Username.
   *
   * @return bool|\Drupal\user\UserInterface
   *   User entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function userLoadByName($username) {
    /** @var \Drupal\user\UserInterface[] $users */
    $users = $this->entityTypeManager()->getStorage('user')->loadByProperties([
      'name' => $username,
      'status' => 1,
    ]);

    return $users ? reset($users) : FALSE;
  }

}
