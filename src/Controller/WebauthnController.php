<?php

namespace Drupal\webauthn_authenticator\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\webauthn_authenticator\Entity\WebauthnCredential;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * WebAuthn list credentials controller.
 */
class WebauthnController extends ControllerBase {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Date formatter.
   */
  public function __construct(DateFormatterInterface $date_formatter) {
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter')
    );
  }

  /**
   * List WebAuthn credentials.
   *
   * @param \Drupal\user\UserInterface|null $user
   *   User entity.
   *
   * @return array
   *   Render array.
   */
  public function userCredentials(UserInterface $user = NULL) {
    $build = $rows = [];
    $webauthn_credentials = WebauthnCredential::findBy([WebauthnCredential::UID => $user->id()]);
    foreach ($webauthn_credentials as $webauthn_credential) {
      $rows[] = [
        $webauthn_credential->getLabel(),
        $this->dateFormatter->format($webauthn_credential->getCreatedTime(), 'custom', 'Y/m/d H:i:s'),
        [
          'data' => [
            '#type' => 'operations',
            '#links' => [
              'delete' => [
                'title' => $this->t('delete'),
                'url' => Url::fromRoute('webauthn_authenticator.delete_credential', [
                  'user' => $user->id(),
                  'webauthn_credential' => $webauthn_credential->id(),
                ]),
              ],
            ],
          ],
        ],
      ];
    }

    $build['credentials'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Label'),
        $this->t('Created'),
        $this->t('Operations'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('Unregistered WebAuthn.'),
    ];

    return $build;
  }

}
