<?php

namespace Drupal\webauthn_authenticator\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\user\UserInterface;
use Drupal\webauthn_authenticator\Repository\PublicKeyCredentialSourceRepository;
use Drupal\webauthn_authenticator\Services\RpServer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Webauthn\PublicKeyCredentialCreationOptions;
use Webauthn\PublicKeyCredentialUserEntity;

/**
 * WebAuthn RegistrationController.
 */
class RegistrationController extends ControllerBase {

  /**
   * WebAuthn RP Server.
   *
   * @var \Drupal\webauthn_authenticator\Services\RpServer
   */
  private $rpServer;

  /**
   * The Private Temp Store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private $tempstore;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\webauthn_authenticator\Services\RpServer $rp_server
   *   WebAuthn RP Server.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore
   *   The private tempstore.
   */
  public function __construct(RpServer $rp_server, PrivateTempStoreFactory $tempstore) {
    $this->rpServer = $rp_server;
    $this->tempstore = $tempstore->get('webauthn_authenticator');
    $this->logger = $this->getLogger('webauthn_authenticator');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webauthn_authenticator.rp_server'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Get Server side Registration options.
   *
   * @param \Drupal\user\UserInterface $user
   *   The registration target user.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Server side registration options.
   */
  public function getRegistrationOption(UserInterface $user) {
    $user_entity = new PublicKeyCredentialUserEntity($user->getAccountName(), $user->id(), $user->getDisplayName());

    $credential_source_repository = new PublicKeyCredentialSourceRepository();
    $credential_sources = $credential_source_repository->findAllForUserEntity($user_entity);
    $exclude_credentials = array_map(function ($credential) {
      /** @var \Webauthn\PublicKeyCredentialSource $credential */
      return $credential->getPublicKeyCredentialDescriptor();
    }, $credential_sources);

    $credential_options = $this->rpServer->generatePublicKeyCredentialCreationOptions($user_entity, PublicKeyCredentialCreationOptions::ATTESTATION_CONVEYANCE_PREFERENCE_NONE, $exclude_credentials);

    try {
      $this->tempstore->set('credential_creation_options', $credential_options);
    }
    catch (\Exception $e) {
      $this->logger->error('Failed to set credentials in tempstore.private.');
    }

    return new JsonResponse($credential_options);
  }

}
