<?php

namespace Drupal\webauthn_authenticator\Repository;

use Drupal\webauthn_authenticator\Entity\WebauthnCredential;
use Webauthn\PublicKeyCredentialSource;
use Webauthn\PublicKeyCredentialSourceRepository as PublicKeyCredentialSourceRepositoryInterface;
use Webauthn\PublicKeyCredentialUserEntity;

/**
 * WebAuthn PublicKeyCredentialSourceRepository.
 */
final class PublicKeyCredentialSourceRepository implements PublicKeyCredentialSourceRepositoryInterface {

  /**
   * {@inheritdoc}
   */
  public function findOneByCredentialId(string $public_key_credential_id): ?PublicKeyCredentialSource {
    $webauthn_credential = WebauthnCredential::findOneBy([WebauthnCredential::CREDENTIAL_ID => base64_encode($public_key_credential_id)]);
    return $this->generatePublicKeyCredentialSource($webauthn_credential);
  }

  /**
   * {@inheritdoc}
   */
  public function findAllForUserEntity(PublicKeyCredentialUserEntity $public_key_credential_user_entity): array {
    $credential_sources = [];

    $webauthn_credentials = WebauthnCredential::findBy([WebauthnCredential::UID => $public_key_credential_user_entity->getId()]);
    foreach ($webauthn_credentials as $webauthn_credential) {
      $credential_sources[] = $this->generatePublicKeyCredentialSource($webauthn_credential);
    }

    return $credential_sources;
  }

  /**
   * {@inheritdoc}
   */
  public function saveCredentialSource(PublicKeyCredentialSource $public_key_credential_source, $label = NULL): void {
    $public_key_credential_id = base64_encode($public_key_credential_source->getPublicKeyCredentialId());
    $webauthn_credential = WebauthnCredential::findOneBy([WebauthnCredential::CREDENTIAL_ID => $public_key_credential_id]);
    if (!$webauthn_credential) {
      $webauthn_credential = WebauthnCredential::create();
    }
    if (!empty($label)) {
      $webauthn_credential->setLabel($label);
    }
    $webauthn_credential->setUid($public_key_credential_source->getUserHandle());
    $webauthn_credential->setCredentialId($public_key_credential_id);
    $webauthn_credential->setCredential(json_encode($public_key_credential_source));
    $webauthn_credential->save();
  }

  /**
   * Generate WebAuthn Public Key Credential Source from WebAuthn Credential.
   *
   * @param \Drupal\webauthn_authenticator\Entity\WebauthnCredential|null $webauthn_credential
   *   WebAuthn Credential entity.
   *
   * @return \Webauthn\PublicKeyCredentialSource|null
   *   Public Key Credential Source.
   */
  private function generatePublicKeyCredentialSource(?WebauthnCredential $webauthn_credential): ?PublicKeyCredentialSource {
    if (!$webauthn_credential) {
      return NULL;
    }

    $credential = json_decode($webauthn_credential->getCredential(), TRUE);
    return PublicKeyCredentialSource::createFromArray($credential);
  }

}
