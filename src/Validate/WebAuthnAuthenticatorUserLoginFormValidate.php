<?php

namespace Drupal\webauthn_authenticator\Validate;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Exception;

/**
 * User Login Form Validation.
 */
class WebAuthnAuthenticatorUserLoginFormValidate {

  /**
   * Verify the credentials, and if the success login, set the uid.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public static function validate(array &$form, FormStateInterface $form_state) {
    $webauthn_publickey = $form_state->getValue('webauthn_authenticator_publickey');
    if (empty($webauthn_publickey)) {
      return;
    }

    try {
      /** @var \Drupal\Core\TempStore\PrivateTempStore $tempstore */
      $tempstore = Drupal::service('tempstore.private')->get('webauthn_authenticator');
      /** @var \Webauthn\PublicKeyCredentialUserEntity $user_entity */
      $user_entity = $tempstore->get('credential_request_user');
      $credential_options = $tempstore->get('credential_request_options');

      /** @var \Drupal\webauthn_authenticator\Services\RpServer $rp_server */
      $rp_server = Drupal::service('webauthn_authenticator.rp_server');

      // Check the validity of the authentication information.
      // Exception is raised in case of failure.
      $rp_server->loadAndCheckAssertionResponse(
        $webauthn_publickey,
        $credential_options,
        $user_entity,
        Drupal::request()
      );

      $form_state->set('uid', $user_entity->getId());
    }
    catch (Exception $e) {
      $form_state->setErrorByName('webauthn_authenticator_registration', t('Failed Certification.'));
    }
  }

}
