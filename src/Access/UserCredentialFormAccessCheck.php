<?php

namespace Drupal\webauthn_authenticator\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Checks access for User credential forms.
 */
class UserCredentialFormAccessCheck implements AccessInterface {

  /**
   * Checks access to any User credential forms.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\user\UserInterface $user
   *   The currently display user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, UserInterface $user = NULL) {
    if ($account->hasPermission('administer users')) {
      return AccessResult::allowed();
    }
    elseif ($account->hasPermission('access webauthn authenticator') && $account->id() === $user->id()) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

}
