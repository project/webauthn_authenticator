<?php

namespace Drupal\webauthn_authenticator\Form;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\webauthn_authenticator\Repository\PublicKeyCredentialSourceRepository;
use Drupal\webauthn_authenticator\Services\RpServer;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Registration WebAuthn Credential Form.
 */
class RegistrationCredentialForm extends FormBase {

  /**
   * User entity.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * WebAuthn RP Server.
   *
   * @var \Drupal\webauthn_authenticator\Services\RpServer
   */
  private $rpServer;

  /**
   * The Private Temp Store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private $tempstore;

  /**
   * The CSRF Token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  private $csrfToken;

  /**
   * Constructor.
   *
   * @param \Drupal\webauthn_authenticator\Services\RpServer $rp_server
   *   WebAuthn RpServer.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempstore
   *   The private tempstore.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrf_token
   *   The CSRF Token generator.
   */
  public function __construct(RpServer $rp_server, PrivateTempStoreFactory $tempstore, CsrfTokenGenerator $csrf_token) {
    $this->rpServer = $rp_server;
    $this->tempstore = $tempstore->get('webauthn_authenticator');
    $this->csrfToken = $csrf_token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webauthn_authenticator.rp_server'),
      $container->get('tempstore.private'),
      $container->get('csrf_token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webauthn_authenticator_registration_credential_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $this->user = $user;

    $form = [];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#size' => 60,
      '#maxlength' => 60,
      '#required' => TRUE,
    ];

    $form['credential'] = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];

    $form['target_uid'] = [
      '#type' => 'hidden',
      '#value' => $user->id(),
    ];

    $form['csrf_token'] = [
      '#type' => 'hidden',
      '#value' => $this->csrfToken->get("user/{$user->id()}/webauthn-authenticator/register/get-registration-options"),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Registration'),
    ];

    $form['#attached']['library'][] = 'webauthn_authenticator/registration';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $label = $form_state->getValue('label');
    $credential = $form_state->getValue('credential');

    try {
      /** @var \Webauthn\PublicKeyCredentialCreationOptions $credential_options */
      $credential_options = $this->tempstore->get('credential_creation_options');

      $credential_source = $this->rpServer->loadAndCheckAttestationResponse($credential, $credential_options, $this->getRequest());
      $credential_source_repository = new PublicKeyCredentialSourceRepository();
      $credential_source_repository->saveCredentialSource($credential_source, $label);

      $this->messenger()->addStatus($this->t('Success registation WebAuthn.'));
      $form_state->setRedirectUrl(Url::fromRoute('webauthn_authenticator.user_credentials', ['user' => $this->user->id()]));
    }
    catch (Exception $e) {
      $this->logger('webauthn_authenticator')->error('@message', ['@message' => $e->getMessage()]);
      $this->messenger()->addError($this->t('Faild registration WebAuthn.'));
    }
  }

}
