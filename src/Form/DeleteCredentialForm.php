<?php

namespace Drupal\webauthn_authenticator\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\webauthn_authenticator\Entity\WebauthnCredential;
use Exception;

/**
 * Delete WebAuthn Credential Form.
 */
class DeleteCredentialForm extends ConfirmFormBase {

  /**
   * The user entity.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The WebAuthn Credential entity.
   *
   * @var \Drupal\webauthn_authenticator\Entity\WebauthnCredential
   */
  protected $webauthnCredential;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webauthn_authenticator_delete_credential_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Delete WebAuthn [@label] ?', ['@label' => $this->webauthnCredential->getLabel()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('webauthn_authenticator.user_credentials', ['user' => $this->user->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL, WebauthnCredential $webauthn_credential = NULL) {
    $this->user = $user;
    $this->webauthnCredential = $webauthn_credential;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $label = $this->webauthnCredential->getLabel();
    try {
      $this->webauthnCredential->delete();
      $this->messenger()->addStatus($this->t('Success delete WebAuthn [@label].', ['@label' => $label]));
      $form_state->setRedirectUrl($this->getCancelUrl());
    }
    catch (Exception $e) {
      $this->messenger()->addError($this->t('Failed delete WebAuthn [@label].', ['@label' => $label]));
    }
  }

}
