<?php

namespace Drupal\webauthn_authenticator\Entity;

use Drupal;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Exception;

/**
 * WebAuthn Credential Entity.
 *
 * @ContentEntityType(
 *   id = "webauthn_credential",
 *   label = @Translation("WebAuthn credential"),
 *   base_table = "webauthn_credential",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   translatable = FALSE
 * )
 */
final class WebauthnCredential extends ContentEntityBase implements ContentEntityInterface {

  public const ENTITY_ID = 'webauthn_credential';
  public const UID = 'uid';
  public const CREDENTIAL_ID = 'credential_id';

  private const LABEL = 'label';
  private const CREDENTIAL = 'credential';

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields[self::LABEL] = BaseFieldDefinition::create('string')
      ->setLabel(t('WebAuthn credential label'))
      ->setDescription(t('The WebAuthn credential label.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    $fields[self::UID] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of the WebAuthn author.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(static::class . '::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    $fields[self::CREDENTIAL_ID] = BaseFieldDefinition::create('string')
      ->setLabel(t('WebAuthn credential ID'))
      ->setDescription(t('The credential ID of the WebAuthn.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    $fields[self::CREDENTIAL] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('WebAuthn credential.'))
      ->setDescription(t('The credential of the WebAuthn.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('WebAuthn status'))
      ->setDescription(t('Whether the WebAuthn is active or blocked.'))
      ->setDefaultValue(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the WebAuthn was created.'))
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the WebAuthn was last edited.'))
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions();
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId(): array {
    return [Drupal::currentUser()->id()];
  }

  /**
   * Get label.
   *
   * @return string
   *   Label.
   */
  public function getLabel(): string {
    return $this->get(self::LABEL)->getString();
  }

  /**
   * Set label.
   *
   * @param string $label
   *   Label.
   */
  public function setLabel($label) {
    $this->set(self::LABEL, $label);
  }

  /**
   * Get UserID.
   *
   * @return string
   *   UserID.
   */
  public function getUid(): string {
    return $this->get(self::UID)->getString();
  }

  /**
   * Set UserID.
   *
   * @param string $uid
   *   UserID.
   */
  public function setUid($uid) {
    $this->set(self::UID, $uid);
  }

  /**
   * Get User entity.
   *
   * @return \Drupal\user\Entity\User
   *   User entity.
   */
  public function getUserEntity() {
    return $this->get(self::UID)->entity;
  }

  /**
   * Get CredentialID.
   *
   * @return string
   *   CredentialID.
   */
  public function getCredentialId(): string {
    return $this->get(self::CREDENTIAL_ID)->getString();
  }

  /**
   * Set CredentialID.
   *
   * @param string $credential_id
   *   CredentialID.
   */
  public function setCredentialId($credential_id) {
    $this->set(self::CREDENTIAL_ID, $credential_id);
  }

  /**
   * Get Credential.
   *
   * @return string
   *   Credential.
   */
  public function getCredential(): string {
    return $this->get(self::CREDENTIAL)->getString();
  }

  /**
   * Set Credential.
   *
   * @param string $credential
   *   Credential.
   */
  public function setCredential($credential) {
    $this->set(self::CREDENTIAL, $credential);
  }

  /**
   * Get created date.
   *
   * @return int
   *   Created date.
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * Get WebAuthn Credential Entity with conditions.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return \Drupal\webauthn_authenticator\Entity\WebauthnCredential[]
   *   WebAuthn Credential entities.
   */
  public static function findBy(array $conditions): array {
    try {
      return Drupal::entityTypeManager()
        ->getStorage(self::ENTITY_ID)
        ->loadByProperties($conditions);
    }
    catch (Exception $e) {
      return [];
    }
  }

  /**
   * Get a WebAuthn Credential Entity with conditions..
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return \Drupal\webauthn_authenticator\Entity\WebauthnCredential|null
   *   WebAuthn Credential entities.
   */
  public static function findOneBy(array $conditions): ?WebauthnCredential {
    return current(self::findBy($conditions)) ?: NULL;
  }

}
