<?php

namespace Drupal\webauthn_authenticator\Services;

use Drupal\webauthn_authenticator\Repository\PublicKeyCredentialSourceRepository;
use Nyholm\Psr7\Factory\Psr17Factory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Webauthn\AuthenticationExtensions\AuthenticationExtensionsClientInputs;
use Webauthn\AuthenticatorSelectionCriteria;
use Webauthn\PublicKeyCredentialCreationOptions;
use Webauthn\PublicKeyCredentialRequestOptions;
use Webauthn\PublicKeyCredentialRpEntity;
use Webauthn\PublicKeyCredentialSource;
use Webauthn\PublicKeyCredentialUserEntity;
use Webauthn\Server;

/**
 * WebAuthn RpServer.
 */
class RpServer {

  /**
   * The WebAuthn Server.
   *
   * @var \Webauthn\Server
   */
  private $server;

  /**
   * RpServer constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $rp_entity = new PublicKeyCredentialRpEntity('webauthn_authenticator', $request_stack->getCurrentRequest()->getHost());
    $public_key_credential_source_repository = new PublicKeyCredentialSourceRepository();
    $this->server = new Server($rp_entity, $public_key_credential_source_repository, NULL);
  }

  /**
   * Generate Server side Public Key Credential Creation Options.
   *
   * @param \Webauthn\PublicKeyCredentialUserEntity $user_entity
   *   The user entity.
   * @param string|null $attestation_mode
   *   The attension mode.
   * @param array $excluded_public_key_descriptors
   *   The exclude public key descriptors.
   * @param \Webauthn\AuthenticatorSelectionCriteria|null $criteria
   *   The criteria.
   * @param \Webauthn\AuthenticationExtensions\AuthenticationExtensionsClientInputs|null $extensions
   *   The extensions.
   *
   * @return \Webauthn\PublicKeyCredentialCreationOptions
   *   The Server side Public key credential creation options.
   */
  public function generatePublicKeyCredentialCreationOptions(
    PublicKeyCredentialUserEntity $user_entity,
    ?string $attestation_mode = PublicKeyCredentialCreationOptions::ATTESTATION_CONVEYANCE_PREFERENCE_NONE,
    array $excluded_public_key_descriptors = [],
    ?AuthenticatorSelectionCriteria $criteria = NULL,
    ?AuthenticationExtensionsClientInputs $extensions = NULL
  ): PublicKeyCredentialCreationOptions {
    return $this->server->generatePublicKeyCredentialCreationOptions($user_entity, $attestation_mode, $excluded_public_key_descriptors, $criteria, $extensions);
  }

  /**
   * Assertion Server and Client Credential and generate credential.
   *
   * @param string $data
   *   Client credential data.
   * @param \Webauthn\PublicKeyCredentialCreationOptions $public_key_credential_creation_options
   *   Server credential options.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Webauthn\PublicKeyCredentialSource
   *   The Public Key Credential Source.
   */
  public function loadAndCheckAttestationResponse(
    string $data,
    PublicKeyCredentialCreationOptions $public_key_credential_creation_options,
    Request $request
  ): PublicKeyCredentialSource {
    $psr_request = $this->convertPsr7Request($request);
    return $this->server->loadAndCheckAttestationResponse($data, $public_key_credential_creation_options, $psr_request);
  }

  /**
   * Generate Server side Public Key Credential Request Options.
   *
   * @param string|null $user_verification
   *   The user verification.
   * @param array $allowed_public_key_descriptors
   *   The allowed public key descriptors.
   * @param \Webauthn\AuthenticationExtensions\AuthenticationExtensionsClientInputs|null $extensions
   *   The extensions.
   *
   * @return \Webauthn\PublicKeyCredentialRequestOptions
   *   The Server side Public key credential request options.
   */
  public function generatePublicKeyCredentialRequestOptions(
    ?string $user_verification = PublicKeyCredentialRequestOptions::USER_VERIFICATION_REQUIREMENT_PREFERRED,
    array $allowed_public_key_descriptors = [],
    ?AuthenticationExtensionsClientInputs $extensions = NULL
  ): PublicKeyCredentialRequestOptions {
    return $this->server->generatePublicKeyCredentialRequestOptions($user_verification, $allowed_public_key_descriptors, $extensions);
  }

  /**
   * Assertion Server and Client Credential.
   *
   * @param string $data
   *   Client credential data.
   * @param \Webauthn\PublicKeyCredentialRequestOptions $public_key_credential_request_options
   *   Sercer credential options.
   * @param \Webauthn\PublicKeyCredentialUserEntity|null $user_entity
   *   The user entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Webauthn\PublicKeyCredentialSource
   *   The public key credential source.
   *
   * @throws \Exception
   */
  public function loadAndCheckAssertionResponse(
    string $data,
    PublicKeyCredentialRequestOptions $public_key_credential_request_options,
    ?PublicKeyCredentialUserEntity $user_entity,
    Request $request
  ): PublicKeyCredentialSource {
    $psr_request = $this->convertPsr7Request($request);
    return $this->server->loadAndCheckAssertionResponse($data, $public_key_credential_request_options, $user_entity, $psr_request);
  }

  /**
   * Convert PSR-7 Request from Symfony Request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Psr\Http\Message\ServerRequestInterface
   *   PSR-7 server Request.
   */
  private function convertPsr7Request(Request $request) {
    $psr17_factory = new Psr17Factory();
    $psr_http_factory = new PsrHttpFactory($psr17_factory, $psr17_factory, $psr17_factory, $psr17_factory);
    return $psr_http_factory->createRequest($request);
  }

}
