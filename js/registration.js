/**
 * @file
 * WebAuthn Authenticator registration functions.
 */

(function ($, Drupal) {

  'use strict';

  var utils = Drupal.behaviors.webauthnAuthenticatorUtils;

  Drupal.behaviors.webauthnRegistration = {
    attach: function (context) {
      $('#webauthn-authenticator-registration-credential-form .js-form-submit', context).once('webauthn-authenticator-registration-click').on('click', function (e) {
        e.preventDefault();

        var targetUid = $('#webauthn-authenticator-registration-credential-form input:hidden[name="target_uid"]').val();
        var csrfToken = $('#webauthn-authenticator-registration-credential-form input:hidden[name="csrf_token"]').val();

        // Obtain the information to register the authentication key from the server.
        $.ajax({
          url: '/user/' + targetUid + '/webauthn-authenticator/register/get-registration-options',
          type: 'GET',
          data: {'token': csrfToken},
          dataType: 'json'
        })
        .then(function (publicKey) {
          // Binary conversion of each key in the response data.
          publicKey.challenge = Uint8Array.from(window.atob(utils.base64url2base64(publicKey.challenge)), function (c) {
            return c.charCodeAt(0);
          });
          publicKey.user.id = Uint8Array.from(window.atob(publicKey.user.id), function (c) {
            return c.charCodeAt(0);
          });
          if (publicKey.excludeCredentials) {
            publicKey.excludeCredentials = publicKey.excludeCredentials.map(function (data) {
              data.id = Uint8Array.from(window.atob(utils.base64url2base64(data.id)), function (c) {
                return c.charCodeAt(0);
              });
              return data;
            });
          }

          // Generate Client credential.
          navigator.credentials.create({publicKey: publicKey})
          .then(function (data) {
            var publicKeyCredential = {
              id: data.id,
              type: data.type,
              rawId: utils.arrayToBase64String(new Uint8Array(data.rawId)),
              response: {
                clientDataJSON: utils.arrayToBase64String(new Uint8Array(data.response.clientDataJSON)),
                attestationObject: utils.arrayToBase64String(new Uint8Array(data.response.attestationObject))
              }
            };

            $('#webauthn-authenticator-registration-credential-form input[name="credential"]').val(JSON.stringify(publicKeyCredential));
            $('#webauthn-authenticator-registration-credential-form').submit();
          })
          .catch(function (error) {
          });
        })
        .catch(function (error) {
          var message = {
            message: Drupal.t('Unable to registration WebAuthn credential. Contact the site administrator if the problem persists.'),
            options: {'type': 'error'}
          };
          if (error['responseText']) {
            var responseText = JSON.parse(error['responseText']);
            message['message'] = responseText['message'];
          }
          utils.showMessages([message]);
        });
      });
    }
  };
})(jQuery, Drupal);
