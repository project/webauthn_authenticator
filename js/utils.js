/**
 * @file
 * WebAuthn Authenticator commom functions.
 */

(function (Drupal) {

  'use strict';

  /**
   * Convert array to base64 string.
   *
   * @param {Uint8Array} a
   *   Target Array.
   *
   * @return {string}
   *   Convert base64 string.
   */
  var arrayToBase64String = function (a) {
    return btoa(String.fromCharCode(...a));
  };

  /**
   * Convert URL encoded base64 string to base64 string.
   *
   * @param {string} input
   *   Target string.
   *
   * @return {string}
   *   Convert base64 string.
   */
  var base64url2base64 = function (input) {
    input = input
      .replace(/=/g, '')
      .replace(/-/g, '+')
      .replace(/_/g, '/');

    var pad = input.length % 4;
    if (pad) {
      if (pad === 1) {
        throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
      }
      input += new Array(5 - pad).join('=');
    }

    return input;
  };

  /**
   * Display JavaScript messages.
   *
   * @param {Array} messages
   *   Messages.
   */
  var showMessages = function (messages) {
    var drupalMessages = new Drupal.Message();
    drupalMessages.clear();

    var messageCount = messages.length;
    for (var i = 0; i < messageCount; ++i) {
      var message = messages[i];
      drupalMessages.add(message['message'], message['options']);
    }
  };

  Drupal.behaviors.webauthnAuthenticatorUtils = {};
  Drupal.behaviors.webauthnAuthenticatorUtils.arrayToBase64String = arrayToBase64String;
  Drupal.behaviors.webauthnAuthenticatorUtils.base64url2base64 = base64url2base64;
  Drupal.behaviors.webauthnAuthenticatorUtils.showMessages = showMessages;
})(Drupal);
