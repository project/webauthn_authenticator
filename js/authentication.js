/**
 * @file
 * WebAuthn Authenticator authentication functions.
 */

(function ($, Drupal) {

  'use strict';

  var utils = Drupal.behaviors.webauthnAuthenticatorUtils;

  Drupal.behaviors.webauthnAuthentication = {
    attach: function (context) {
      $('#user-login-form #edit-submit', context).once('webauthn-authenticator-click').on('click', function (e) {
        $('#webauthn_authenticator_publickey').val('');
        var username = $('#user-login-form #edit-name').val();
        if (!username) {
          return;
        }

        if ($('#user-login-form #edit-pass').val().length > 0) {
          return;
        }

        // Stop event handling.
        e.preventDefault();

        $.ajax({
          url: '/webauthn-authenticator/get-authentication-options',
          type: 'GET',
          data: {
            username: username
          },
          dataType: 'json'
        })
        .then(function (publicKey) {
          // Binary conversion of each key in the response data.
          publicKey.challenge = Uint8Array.from(window.atob(utils.base64url2base64(publicKey.challenge)), function (c) {
            return c.charCodeAt(0);
          });
          if (publicKey.allowCredentials) {
            publicKey.allowCredentials = publicKey.allowCredentials.map(function (data) {
              data.id = Uint8Array.from(window.atob(utils.base64url2base64(data.id)), function (c) {
                return c.charCodeAt(0);
              });
              return data;
            });
          }

          navigator.credentials.get({publicKey: publicKey})
          .then(function (data) {
            var publicKeyCredential = {
              id: data.id,
              type: data.type,
              rawId: utils.arrayToBase64String(new Uint8Array(data.rawId)),
              response: {
                authenticatorData: utils.arrayToBase64String(new Uint8Array(data.response.authenticatorData)),
                clientDataJSON: utils.arrayToBase64String(new Uint8Array(data.response.clientDataJSON)),
                signature: utils.arrayToBase64String(new Uint8Array(data.response.signature)),
                userHandle: data.response.userHandle ? utils.arrayToBase64String(new Uint8Array(data.response.userHandle)) : null
              }
            };
            $('#webauthn_authenticator_publickey').val(JSON.stringify(publicKeyCredential));
            $('#user-login-form').submit();
          })
          .catch(function (error) {
          });
        })
        .catch(function (error) {
          var message = {
            message: Drupal.t('Unable to WebAuthn authentication. Contact the site administrator if the problem persists.'),
            options: {'type': 'error'}
          };
          if (error['responseText']) {
            var responseText = JSON.parse(error['responseText']);
            message['message'] = responseText['message'];
          }
          utils.showMessages([message]);

        });
      });
    }
  };
})(jQuery, Drupal);
